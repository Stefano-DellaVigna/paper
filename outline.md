# 连享会·五一论文班

&#x26F3; [最新版课纲 PDF](https://file.lianxh.cn/KC/lianxh_paper.pdf) | [课程主页](https://gitee.com/lianxh/paper) &#x1F449; <https://gitee.com/lianxh/paper>

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/2022-%E4%BA%94%E4%B8%80%E8%AE%BA%E6%96%87%E7%8F%AD-%E8%AF%8D%E4%BA%91-01.png)

<div STYLE="page-break-after: always;"></div>

# 1. 课程概览

> **时间：** 2022 年 5 月 2-4 日 (9:00-12:00; 14:30-17:30, 半小时答疑)  
> **方式：** 网络直播+回放  
> **嘉宾：** 连玉君 (中山大学)；申广军(中山大学)； 杨海生(中山大学)  
> **课程主页：** <https://gitee.com/lianxh/paper> &#x2B55; [PDF课程大纲](https://file.lianxh.cn/KC/lianxh_paper.pdf)   
> **报名链接：** <http://junquan18903405450.mikecrm.com/6sFqyrI>

## 授课嘉宾

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E8%BF%9E%E7%8E%89%E5%90%9B-%E7%94%9F%E6%B4%BB%E7%85%A7-001.png)   
**[连玉君](https://lingnan.sysu.edu.cn/faculty/lianyujun)** ，中山大学岭南学院副教授，博士生导师。毕业于西安交通大学金禾经济研究，获经济学博士学位。研究方向为公司金融和金融计量，研究兴趣包括公司治理、现金持有、股权质押、投融资行为，成果见诸于 China Economic Review、《经济研究》、《管理世界》、《经济学(季刊)》、《金融研究》、《统计研究》、《世界经济》等期刊，主持国家自然科学基金 2 项。

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E7%94%B3%E5%B9%BF%E5%86%9B-%E5%B7%A5%E4%BD%9C%E7%85%A7-01-180.png)   
**[申广军](https://lingnan.sysu.edu.cn/faculty/shenguangjun)**，经济学博士，中山大学岭南学院经济学副教授。研究兴趣为发展经济学、劳动经济学和公共经济学。论文发表于《经济研究》、《管理世界》、《经济学 (季刊) 》和《金融研究》等中文权威期刊，以及 China Economic Review、Economics Letters、Economics of Transition、International Labour Review、Review of Economic Design 等英文期刊。主持国家自然科学基金、教育部人文社科基金等科研项目。所著论文曾获得第三届刘诗白经济学奖 (2016 年) 、第五届洪银兴经济学奖 (2020 年) 。

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/杨海生100.jpg)   
**[杨海生](https://lingnan.sysu.edu.cn/faculty/yanghaisheng)**，中山大学岭南学院经济学系副教授，主要研究领域为复杂网络、机器学习及因果推断。在 ACM COMPUTING SURVEYS、Emerging Markets Review、 Economic Geography、Ecological Economics、《经济研究》、《管理世界》、《经济学 (季刊) 》、《管理科学学报》、《金融研究》、《会计研究》、《世界经济》等学术刊物上发表多篇论文，主持和参与多项国家自然科学基金、广东省自然科学基金等课题研究。

&emsp;

## 课程导引

「杀手的身价决定于什么？」既非高深的武功，亦非锋利的兵器，而是「杀了谁」？

学术研究也是如此，论文的价值在很大程度上取决于你解决了什么问题，与之相伴的是一系列逻辑严密分析和论证，或曰「研究设计」。研读已经发表的论文通常只能看到作者研究工作的最终成果，而具体的思考、分析和打磨过程，则不得而知，而这恰恰是多数人渴望学习的。

为此，本次专题课程中，三位嘉宾不仅仅是分享论文写作过程中「风光」的一面，也会倾诉「心酸」的体验。这次讲解的论文有近半数为嘉宾自己的论文，便源于这个考虑。

我们将通过精讲和重现 9 篇发表于 Top 期刊上的论文 (AER、QJE、AEJ、JF、JFE，经济研究，经济学季刊)，与大家分享提升研究设计和论文写作能力的经验。课程中涉及的多数论文都会详细讲解实证研究的各个环节，包括：选题、研究设计、数据处理、内生性问题、稳健性检验、结果的解读和可视化呈现，以及研究贡献的梳理等。在此基础上，进一步结合前期和后续文献，讨论其局限和可能的选题方向，以启发学员思考新的选题方向。

课程中涉及**多种计量方法**的综合应用，包括：
- 固定效应模型、高维固定效应模型、长差分
- IV 估计
- 多期倍分法 (DID)、合成控制法、合成 DID
- 分布滞后模型、事件研究法、多元 Logit
- 金融网络效应、稳健性检验、安慰剂检验
- ……

涵盖的**话题**包括：
- 税收与创新行为
- 社会关系与政府官员选任
- 增值税改革
- 经济开发区与产业结构
- 营商环境和商事制度改革
- 金融网络和贸易网络的设定和应用

**研究设计**方面的主题包括：
- 如何设计研究框架？
- 如何优化文档结构和代码 (提高效率-保证可复现)？
- 如何实现「实证结果可视化」？
- 如何论证「研究贡献」？
- 如何进行短期效应和长期效应的估算？
- 如何综合使用多种方法应对「内生性」，确保「稳健性」？

这些主题与大家正在研究或关注的话题存在密切的关联，有助于结合或迁移。

<div STYLE="page-break-after: always;"></div>

# 2. 专题介绍

三天的课程共讲解 9 篇论文 (带 &#x2B55; 标记)，每篇论文均提供完整的重现数据和代码。

- 授课顺序
  - 连玉君 &ensp; 5 月 2 日
  - 申广军 &ensp; 5 月 3 日
  - 杨海生 &ensp; 5 月 4 日
  
  &emsp; 

- &#x26F3; [浏览所有论文](https://www.jianguoyun.com/p/DbU1jrIQtKiFCBi9v7ME)

&emsp;

## &#x2B31; T1：研究设计专题

### G1：社会关系与官员选任 (3 小时)
&#x2B55; A1. Fisman2020
> Fisman, R., J. Shi, Y. Wang, W. Wu, **2020**, Social ties and the selection of China’s political elite, **American Economic Review**, 110 (6): 1752-1781. [-Link-](https://doi.org/10.1257/aer.20180841), [-PDF1-](https://sci-hub.ren/10.1257/aer.20180841), [-cited-](https://xs2.dailyheadlines.cc/scholar?cites=15150378991626950623&as_sdt=2005&sciodt=0,5&hl=zh-CN), [-Replication-](https://www.openicpsr.org/openicpsr/project/115794/version/V1/view)
- **简介：** 直觉而言，与现任成员具有「同乡」或「大学校友」关系的候选人更容易入选。然而，这篇针对中央政治局成员选任的论文却发现了截然相反的结果：同乡或校友背景会导致候选人入选的概率下降 5-9 个百分点，并称之为「人脉惩罚」。文章在多种设定下证实了上述结果的稳健性，进而对三种「排他性解释」进行了检验，为上述结果提供了合理的经济解释。   
该文一方面与前期的政治关联、社会网络、社会关系等主题的文献一脉相承，另一方面也为研究政府治理、官员选任以及地方政府业绩等话题提供了很好的视角，具有不少扩展方向。 
- **学习要点：** 
  - 如何论述和检验「排他性」解释？
  - 高维固定效应的作用和解释
  - 线性概率模型 v.s. Logit/Probit  
  - 如何采用异质性分析强化你的观点
  - 顺藤摸瓜：结合该文的 [引文](https://xs2.dailyheadlines.cc/scholar?cites=15150378991626950623&as_sdt=2005&sciodt=0,5&hl=zh-CN)，讨论可能的扩展方向

### G2：税收与创新 (3 小时)
&#x2B55; A2. Akcigit2022  
> [Akcigit](http://www.ufukakcigit.com/), U., J. Grigsby, T. Nicholas, S. Stantcheva, **2022**, Taxation and innovation in the twentieth century, **The Quarterly Journal of Economics**, 137 (1): 329-385. [-Link-](https://doi.org/10.1093/qje/qjab022), [-PDF-](https://file-lianxh.oss-cn-shenzhen.aliyuncs.com/Refs/Paper2022/Akcigit-2022-QJE.pdf), [-Appendix-](https://oup.silverchair-cdn.com/oup/backfile/Content_public/Journal/qje/137/1/10.1093_qje_qjab022/1/qjab022_onlineappendix.pdf?Expires=1649965153&Signature=soPrpxU1GGqhdNr58Nc6j-gZhttwWtj2XQG5WxaVp-k7ZqVAJOoYz60biLwCcpgYVpVutAw-uJn59pJkQJOuZlMv6DHHRPiIHE2I7CUNOv5c05r1msRmBbFmzntnyXBov2UkywbuJpob1e59Q5fesu5Z7t6RQFOoh8qgVxjlQcTNgcN6YFuFISMPa2GP8zRbQcNxcFuKbRhPyoUMqFI-MJkwVS7pfl162hJ0ZRa0fH9ho7N3FhBGyoN0jAufE1S3vCSeb2FetG7lhS8JGYMb~FMOcpyRpv1hjSiSL52lSl5W2jT18i1uN-k4atVdt3TN-JFWXMk796qn~BvYnM~85Q__&Key-Pair-Id=APKAIE5G5CRDK6RD3PGA), [-cited-](https://xs2.dailyheadlines.cc/scholar?cites=3691559864871282829&as_sdt=2005&sciodt=0,5&hl=zh-CN&scioq=Social+ties+and+the+selection+of+China%27s+political+elite), [-Replication-](https://doi.org/10.7910/DVN/SR410I)
- **简介：** 文章研究了美国公司税和个人税对创新的影响。作者将发明人数据库、公司税率数据库，以及州级个人所得税和其他经济数据关联起来，从宏观和微观两个层面估计了税收对创新 (数量、质量、发生地等) 的影响。文中采用了多种识别策略，得到了非常一致的结论：(1) 高税率对创新的数量和发生地具有负面影响，但不会影响平均创新质量；(2) 州级层面的「税收-创新产出」弹性很大；(3) 公司税主要影响受雇发明者 (相对于自由职业者) 的创新产出和跨州流动性；而个人所得税则会对整体创新数量和发明人的流动性产生影响。

- **学习要点：** 
  - **研究设计和写作。** 该文涉及宏观和微观两个视角、各类创新行为的衡量方法、不同的税率指标、短期效应和长期效应估计等多个方面，但全文始终以产出弹性为主线，采用多种识别策略，从不同角度来论证结果的合理性及经济含义，严格遵守了「一个问题一次性说清楚」的原则。
  - **研究贡献和结果解读。** 这是论文中至关重要但却不容易写好的部分。作者的策略是「对标」——通过与前期 Top 期刊文献的细致对比来论述其贡献，通过与同类或相近文献对比来说明其估计结果的合理性。
  - **实证研究方法的组合。** 全文使用了近 10 种不同的计量方法，比如「高维固定效应模型」、「交互固定效应」、「长差分」、「事件研究法」、「合成控制+DID」、「分布滞后模型」、「固定效应多元 Logit」等。你能够感觉到作者是信手拈来，因为，每一种方法的应用都恰到好处，虽然难度不大，但能够很好地回答「产出弹性的时变性和异质性」、「政策的短期和长期效应」等比较棘手的问题。 
  - **文章的附录如何做？** 作者提供了一份 70 多页的「[Appendix](https://oup.silverchair-cdn.com/oup/backfile/Content_public/Journal/qje/137/1/10.1093_qje_qjab022/1/qjab022_onlineappendix.pdf?Expires=1649965153&Signature=soPrpxU1GGqhdNr58Nc6j-gZhttwWtj2XQG5WxaVp-k7ZqVAJOoYz60biLwCcpgYVpVutAw-uJn59pJkQJOuZlMv6DHHRPiIHE2I7CUNOv5c05r1msRmBbFmzntnyXBov2UkywbuJpob1e59Q5fesu5Z7t6RQFOoh8qgVxjlQcTNgcN6YFuFISMPa2GP8zRbQcNxcFuKbRhPyoUMqFI-MJkwVS7pfl162hJ0ZRa0fH9ho7N3FhBGyoN0jAufE1S3vCSeb2FetG7lhS8JGYMb~FMOcpyRpv1hjSiSL52lSl5W2jT18i1uN-k4atVdt3TN-JFWXMk796qn~BvYnM~85Q__&Key-Pair-Id=APKAIE5G5CRDK6RD3PGA)」文档，基本上涵盖了我们所能想象到的各种 **稳健性** 检验方法，可以作为我们在投稿时制作附录文档的范本。
  - **如何扩展？** 该文的四位作者，尤其是第一作者做了一系列主题相近的论文，都有很高的引用率，这篇文章本身也被后续学者广泛[引用](https://doi.org/10.1093/qje/qjab022)，这为我们在此文基础上选择自己的切入视角提供了很多启发。


&emsp;

## &#x2B31; T2：投中文还是英文？

越来越多的国内学者开始「两栖作战」，这也是未来的趋势。那么，同样一个话题，中文稿和英文稿在研究设计、写作等方面有何差别？

申广军老师将通过深入对比两组主题相似的文章来解读这个问题。

- 第一，从研究视角、文献切入、研究方法、数据处理、分析过程、结果呈现等多个角度比较文章的差异，尝试回答「**好文章好在哪？**」的问题。
- 第二，通过回顾文章的研究历程、讲解研究设计的基本原理、拆解重现文章核心结果等方式，尝试回答「**如何写好文章？**」的问题。


### G1：税收政策评估 (3 小时)

评估税收政策的两篇文章。首先介绍双重差分模型 (DID) 的一般原理，然后以两篇文章为例，讲解 DID 方法在政策评估中的典型应用。讲解过程中，从研究视角、研究方法、数据处理、分析过程、结果呈现等多个角度探索中文期刊与英文期刊的差异。
- DID、DID-IV 的估计及 STATA 实现
- 传统 DID 的问题和解决方案
- &#x2B55; **B1. 申广军2016**
  - 申广军、陈斌开、杨汝岱，**2016**. 减税能否提振中国经济——基于中国增值税改革的实证研究. **经济研究**，2016 (11): 70-82. [-Link-](https://www.cnki.com.cn/Article/CJFDTotal-JJYJ201611007.htm)
- &#x2B55; **B2. Liu2019**
  - Liu, Y., J. Mao, **2019**, How do tax incentives affect investment and productivity? Firm-level evidence from china, **American Economic Journal: Economic Policy**, 11 (3): 261-291. [-Link-](https://doi.org/10.1257/pol.20170478), [-PDF-](https://sci-hub.ren/10.1257/pol.20170478), [-cited-](https://xs2.dailyheadlines.cc/scholar?cites=2581651808817841752&as_sdt=2005&sciodt=0,5&hl=zh-CN&scioq=Social+ties+and+the+selection+of+China%27s+political+elite) 

### G2：开发区的经济影响 (3 小时)

评估产业政策 (经济开发区) 的两篇文章。首先介绍文献中应用 DID 方法时经常遇到的问题，然后讲解常见的解决方案，包括 PSM-DID 和 RDD-DID 等。此外，还将介绍 DID 方法的最新进展及其 Stata 实现。
- DID 的设定及应用场景
- DID 与 PSM、RDD 的联合应用
- DID 的最新进展
- &#x2B55; **B3. 李力行2015**
  - 李力行、申广军，2015：“经济开发区，地区比较优势与产业结构调整”，《经济学 (季刊) 》，第 11 卷第 3 期，第 885-910 页。[-PDF-](http://ww2.usc.cuhk.edu.hk/PaperCollection/webmanager/wkfiles/2012/201510_04_paper.pdf)
- &#x2B55; **B4. Lu2019**
  - Lu, Y., J. Wang, L. Zhu, **2019**, Place-based policies, creation, and agglomeration economies: Evidence from china's economic zone program, **American Economic Journal: Economic Policy**, 11 (3): 325-360. [-Link-](https://www.aeaweb.org/articles?id=10.1257/pol.20160272), [-cited-](https://xs2.dailyheadlines.cc/scholar?cites=10022787061135373201&as_sdt=2005&sciodt=0,5&hl=zh-CN), [-PDF-](http://sci-hub.ren/10.1257/pol.20160272), [-Appendix-](https://assets.aeaweb.org/asset-server/files/8358.pdf)

&emsp;

## &#x2B31; T3：面板变系数模型-合成DID-金融网络模型

本讲精讲三篇文献，帮助大家了解并掌握 **MO-OLS**、**Synthetic DID** 以及 **金融网络模型** 的实证方法与应用。课程中提供的 R+ Stata 复现代码和程序能帮助大家深入了解这些看似复杂的方法背后的原理和实操中的各类陷阱。

更为重要的是，杨老师还将介绍这些方法和模型的各类拓展及其在金融、国际贸易、环境经济学、财政以及区域经济学等领域中的应用，以便大家将这些方法有机地嵌入自己的研究中。 

各个专题的具体介绍如下：

### G1: MO-OLS (2 小时)
**概要：** 本专题介绍「面板变系数模型 (MO-OLS)」的设定思想、应用场景及 Stata 实操。该方法允许估计系数随个体或随时间发生变化，即从传统的 $\widehat{\beta}$ 扩展到 $\widehat{\beta}_i$, $\widehat{\beta}_t$, 甚至 $\widehat{\beta}_{it}$，使「截面异质性」、「政策效应时变性」等问题的分析变得更有说服力。
- 面板变系数模型的设定及应用场景
- MO-OLS 的估计及 STATA 实现
- &#x2B55; **C1. Keane2020** (精讲论文)
  - Keane, Michael, and Timothy Neal. 2020. “Climate change and US agriculture: Accounting for multidimensional slope heterogeneity in panel data.” **Quantitative Economics** 11 (4): 1391-1429. [-Link-](https://onlinelibrary.wiley.com/doi/full/10.3982/QE1319), [-PDF-](https://onlinelibrary.wiley.com/doi/epdf/10.3982/QE1319), [-Appendix-](https://onlinelibrary.wiley.com/action/downloadSupplement?doi=10.3982%2FQE1319&file=quan200109-sup-0001-onlineappendix.pdf), [-Replication-](https://onlinelibrary.wiley.com/action/downloadSupplement?doi=10.3982%2FQE1319&file=quan200109-sup-0002-dataandprograms.zip)
- **扩展应用：** 
  - Carpenter, J. N., F. Lu, R. F. Whitelaw, **2021**, The real value of china’s stock market, **Journal of Financial Economics**, 139 (3): 679-696. [-Link-](https://doi.org/10.1016/j.jfineco.2020.08.012), [-PDF1-](https://sci-hub.ren/10.1016/j.jfineco.2020.08.012), [PDF2](https://www.sciencedirect.com/science/article/pii/S0304405X20302403/pdfft?md5=39e3a48af91e6ec605313578c0fb7146&pid=1-s2.0-S0304405X20302403-main.pdf), [-cited-](https://xs2.dailyheadlines.cc/scholar?cites=3717038521455364586&as_sdt=2005&sciodt=0,5&hl=zh-CN)

### G2: Synthetic DID (2 小时)
**概要：** 介绍由 Athey 和 Imbens 等大牛们于 2021 年提出的「合成 DID」(Synthetic DID)。该方法可以视为「合成控制法」与「倍分法」的结合体，克服了各自的局限：它可以估计实验组包含多个受试对象的情形，又能克服倍分法中的平行趋势检验难题。该方法将成为政策评价工具箱中的重要一员。杨老师本人有两篇应用此方法的论文已经被经济学 (季刊) 和经济研究接受。杨老师结合「商事制度改革」这一政策，使用 R 软件来讲解 Synthetic DID 的估计方法和应用要点。
- Synthetic DID 的设定及应用场景
- Synthetic DID 的 R 语言实现
- &#x2B55; **C2. 毕青苗2022** (精讲论文)
  - 毕青苗, 徐现祥, 杨海生. 新增就业创新高之谜：基于商事制度改革的视角. **2022**, 工作论文. (PDF 版本将在课件中提供) [](https://file-lianxh.oss-cn-shenzhen.aliyuncs.com/Refs/Paper2022/%E6%AF%95%E9%9D%92%E8%8B%97-2022-%E6%96%B0%E5%A2%9E%E5%B0%B1%E4%B8%9A%E5%A2%9E%E9%95%BF%E4%B9%8B%E8%B0%9C%EF%BC%9A%E5%9F%BA%E4%BA%8E%E5%95%86%E4%BA%8B%E5%88%B6%E5%BA%A6%E6%94%B9%E9%9D%A9%E7%9A%84%E8%A7%86%E8%A7%92.pdf) 
- **延伸阅读：** 
  - Arkhangelsky, D., S. Athey, D. A. Hirshberg, G. W. Imbens,S. Wager, **2021**, Synthetic difference-in-differences, **American Economic Review**, 111 (12): 4088-4118. [-Link-](https://doi.org/10.1257/aer.20190159), [-PDF-](https://file-lianxh.oss-cn-shenzhen.aliyuncs.com/Refs/Paper2022/Arkhangelsky-2021-Synthetic-Difference-In-Differences.pdf) , [-Appendix-](https://www.aeaweb.org/doi/10.1257/aer.20190159.appx), [-cited-](https://xs2.dailyheadlines.cc/scholar?cites=380000139855607810&as_sdt=2005&sciodt=0,5&hl=zh-CN)

### G3: 金融网络模型实证方法 (2 小时)
**概要：** 通过 Kondor et al. (2022) 的 Stata 实操，介绍近几年备受关注的网络效应，包括：金融网络模型的设定和估计方法；应用场景及其与传统方法的关系和区别；网络特征及网络内生性问题的检验和估计。最后利用发表于 JF 和经济学 (季刊) 的两篇文章来介绍该模型在金融领域的应用及可能的扩展方向。
- 金融网络模型的设定及应用场景
- 金融网络模型的 Stata 实现
- &#x2B55; **C3. Kondor2021** (精讲论文)
  - Kondor, P., G. PintÉR, **2021**, Clients' connections: Measuring the role of private information in decentralized markets, **The Journal of Finance**, 77 (1): 505-544. [-Link-](https://doi.org/10.1111/jofi.13087), [-PDF-](https://onlinelibrary.wiley.com/doi/epdf/10.1111/jofi.13087), [-Appendix-](https://onlinelibrary.wiley.com/action/downloadSupplement?doi=10.1111%2Fjofi.13087&file=jofi13087-sup-0001-InternetAppendix.pdf), [-Replication-](https://onlinelibrary.wiley.com/action/downloadSupplement?doi=10.1111%2Fjofi.13087&file=jofi13087-sup-0002-ReplicationCode.zip), [-cited-](https://xs2.dailyheadlines.cc/scholar?cites=6978622187582691447&as_sdt=2005&sciodt=0,5&hl=zh-CN)
- **扩展应用：** 
  - Li, D. A. N., N. SchÜRhoff, **2018**, Dealer networks, **The Journal of Finance**, 74 (1): 91-144. [-Link-](https://doi.org/10.1111/jofi.12728), [-PDF1-](https://sci-hub.ren/10.1111/jofi.12728), [PDF2](https://onlinelibrary.wiley.com/doi/epdf/10.1111/jofi.12728), [-Appendix-](https://onlinelibrary.wiley.com/action/downloadSupplement?doi=10.1111%2Fjofi.12728&file=jofi12728-sup-0001-InternetAppendix.pdf), [-cited-](https://xs2.dailyheadlines.cc/scholar?cites=11223075374532523330&as_sdt=2005&sciodt=0,5&hl=zh-CN)
  - 周开国，邢子煜，杨海生，《全球贸易网络与股票市场国际影响力》，《经济学 (季刊) 》，2022，已接收. [-PDF-](https://file-lianxh.oss-cn-shenzhen.aliyuncs.com/Refs/Paper2022/%E5%91%A8%E5%BC%80%E5%9B%BD-2022-%E7%BB%8F%E6%B5%8E%E5%AD%A6%E5%AD%A3%E5%88%8A.pdf) 

&emsp;

> &#x26F3; [浏览所有论文](https://www.jianguoyun.com/p/DbU1jrIQtKiFCBi9v7ME)

<div STYLE="page-break-after: always;"></div>

# 3. 听课指南

## 软件和课件
- **课件/计量软件：** Stata，提供全套 Stata 实操程序、数据和 dofiles (开课前一周发送)。建议使用 Stata 16.0 或更高版本。(Note: 杨海生老师讲解合成 DID 时需使用 R 软件)
- **听课软件**：本次课程可以在手机，ipad 以及 windows 系统的电脑上听课。
  - **特别提示**：一个账号绑定一个设备，且听课电脑不能外接显示屏，请大家提前准备好自己的听课设备。  
  
## 实名制报名
本次课程实行实名参与，具体要求如下：   
- 高校老师/同学报名时需要向连享会课程负责人 **提供真实姓名，并附教师证/学生证图片**；
- 研究所及其他单位报名需提供 **能够证明姓名以及工作单位的证明**；
- 报名即默认同意「[<font color=darkred>**连享会版权保护协议条款**</font>](https://www.lianxh.cn/news/b16b512ee620b.html)」。

&emsp;

# 4. 报名和缴费信息

- **主办方：** 太原君泉教育咨询有限公司
- **标准费用** (含报名费、材料费)：2800 元/人 (全价)
- **优惠方案**：
  - 三人及以上团购/专题课老学员：9折，2520 元/人
  - 学生（需提供学生证/卡照片）：9折，2520 元/人
  - 充值 [连享会会员](https://www.lianxh.cn/news/fd8a627666568.html)：85折 2380 元/人
  - **温馨提示：** 以上各项优惠不能叠加使用。
- **联系方式：**
  - 邮箱：[wjx004@sina.com](wjx004@sina.com)
  - 电话 (微信同号)： 王老师 18903405450 ; 李老师 18636102467


## 报名链接

> **报名链接：**
> <http://junquan18903405450.mikecrm.com/6sFqyrI>

> 或 长按/扫描二维码报名：

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/报名：五一论文班.png)


## 缴费方式

> **方式 1：对公转账**

- 户名：太原君泉教育咨询有限公司
- 账号：35117530000023891 (晋商银行股份有限公司太原南中环支行)
- **温馨提示：** 对公转账时，请务必提供「**汇款人姓名-单位**」信息，以便确认。

> **方式 2：微信扫码支付**

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/君泉收款码.png)

> **温馨提示：** 微信转账时，请务必在「添加备注」栏填写「**汇款人姓名-单位**」信息。

<div STYLE="page-break-after: always;"></div>

# 5. 诚聘助教 &#x26BD;

> ## 说明和要求

- **名额：** 9 名
- **任务：**
  - **A. 课前准备**：协助完成 2 篇推文，风格类似于 [lianxh.cn](https://www.lianxh.cn) ；
  - **B. 开课前答疑**：协助学员安装课件和软件，在微信群中回答一些常见问题；
  - **C. 上课期间答疑**：针对前一天学习的内容，在微信群中答疑 (8:00-9:00，19:00-22:00)；参见 [往期答疑](https://gitee.com/arlionn/PX/wikis/README.md?sort_id=3372005)。
  - Note: 下午 5:30-6:00 的课后答疑由主讲教师负责。
- **要求：** 热心、尽职，熟悉 Stata 的基本语法和常用命令，能对常见问题进行解答和记录。
- **特别说明：** 往期按期完成任务的助教自动获得本期助教资格，不必填写申请资料，直接联系连老师即可。
- **截止时间：** 2022 年 4 月 15 日 (将于 4 月 17 日公布遴选结果于连享会主页 [lianxh.cn](https://www.lianxh.cn) 或课程主页 <https://gitee.com/lianxh/paper>)

> **申请链接：** <https://www.wjx.top/vj/OtcwrLF.aspx>

> 或扫码填写助教申请资料：

> ![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/助教招聘—2021论文班.png)

> **课程主页：** <https://gitee.com/lianxh/paper>

&emsp;

<div STYLE="page-break-after: always;"></div>

# 更多课程

>## 免费公开课  

- [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) - 连玉君，时长：1小时40分钟，[课程主页](https://gitee.com/arlionn/PanelData)，[Bilibili 站版](https://www.bilibili.com/video/BV1oU4y187qY)
- [Stata 33 讲](http://lianxh-pc.duanshu.com/course/detail/b22b17ee02c24015ae759478697df2a0) - 连玉君, 每讲 15 分钟. [课程主页](https://gitee.com/arlionn/stata101)，[课件](https://gitee.com/arlionn/stata101)，[Bilibili 站版](https://space.bilibili.com/546535876/channel/detail?cid=160748)    
- 部分直播课 [课程资料下载](https://gitee.com/arlionn/Live) (PPT，dofiles等) 

> ## [计量专题课程](https://www.lianxh.cn/news/46917f1076104.html) &#x26F3; 

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhbottom01.png)](https://www.lianxh.cn/news/46917f1076104.html)

&emsp; 

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhbottom03.png)](https://www.lianxh.cn/news/46917f1076104.html)


<div STYLE="page-break-after: always;"></div>

## 关于我们

- **Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。
- [连享会-主页](https://www.lianxh.cn) 和 [知乎专栏](https://www.zhihu.com/people/arlionn/)，700+ 推文，实证分析不再抓狂。[直播间](http://lianxh.duanshu.com) 有很多视频课程，可以随时观看。
- <font color=red>New！</font> **`lianxh` 命令发布了：**    
  - 随时搜索连享会推文、Stata 资源，安装命令如下：  
  - &emsp; `. ssc install lianxh`  
  - 使用详情参见帮助文件 (有惊喜)：   
  - &emsp; `. help lianxh`

&emsp; 

> 连享会小程序：扫一扫，看推文，看视频……

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会小程序二维码180.png)](https://www.lianxh.cn/news/46917f1076104.html)

&emsp; 

> 扫码加入连享会微信群，提问交流更方便

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会-学习交流微信群001-150.jpg)  

